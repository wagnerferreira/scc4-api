package com.scc4.utils;

import com.scc4.utils.Fracao;

public class Calculadora {

    public Fracao soma(Fracao f1, Fracao f2) {
        return f1.soma(f2);
    }

    public Fracao subtracao(Fracao f1, Fracao f2) {
        return f1.subtracao(f2);
    }

    public Fracao divisao(Fracao f1, Fracao f2) {
        return f1.divisao(f2);
    }

    public Fracao multiplicao(Fracao f1, Fracao f2) {
        return f1.multiplicao(f2);
    }

}